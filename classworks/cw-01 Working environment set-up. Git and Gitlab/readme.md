# GIT

- клонуємо репозиторій - `git clone https://gitlab.com/dan-it/groups/pe-27.git`
- оновлюємо репозиторій з хмари - `git pull`

## Відправити зміни в локальному репозтиторії до хмари

- фіксуємо зміни - `git add .`
- підписисуємо зміни - `git commit -m "changes description"`
- штовхаємо в хмару - `git push`

### Для того, щоб побачити кольорові коментарі встановть в редактор коду доповнення better comments

https://plugins.jetbrains.com/plugin/10850-better-comments
https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments
